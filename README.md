# Felix-user-app
This repository contains an application capable of sending and receiving data using felix-client.
The application is not compiled against FELIX source code; the FELIX libraries are loaded dynamically at run time.

### TDAQ dependencies
The application depends on the following TDAQ packages:
* [felix_proxy](https://gitlab.cern.ch/atlas-tdaq-software/felix_proxy), a library to load the necessary FELIX shared library. Similar to felix-client-thread in the FELIX software repository.
* [felix-interface](https://gitlab.cern.ch/atlas-tdaq-software/felix-interface), the set of felix-client-thread API headers.

### Felix release
LD_LIBRARY_PATH has to contain the location of the FELIX libraries (of a given release), for example
```
/cvmfs/atlas-online-nightlies.cern.ch/felix/releases/felix-05-00-02-rm5-stand-alone/x86_64-centos7-gcc11-opt/lib`
```
The FELIX releases can be found at:
- CVMFS (see example above)
- `/sw/atlas/tdaq/felix/` in P1


## Quick start

1. create a new directory
2. create a TDAQ-like CMakeLists.txt
```
echo -e "cmake_minimum_required(VERSION 3.14.0)
project(FELIX-USER-APP)
find_package(TDAQ)
include(CTest)
tdaq_work_area(tdaq 10.0.0)" > CMakeLists.txt
```
3. clone this repository
4. compile the application
```
alias cm_setup='source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh'
cm_setup tdaq-10-00-00 x86_64-centos7-gcc11-opt
cmake_config x86_64-centos7-gcc11-opt
cd x86_64-centos7-gcc11-opt  
make && make install
cd ../
source installed/setup.sh
```
5. `export LD_LIBRARY_PATH=/cvmfs/atlas-online-nightlies.cern.ch/felix/releasesfelix-05-00-02-rm5-stand-alone/x86_64-centos7-gcc11-opt/lib:$LD_LIBRARY_PATH`

6. the executable can be found in `installed/x86_64-centos7-gcc11-opt/bin` or `x86_64-centos7-gcc11-opt/felix-user-app`

7. `./felix-user-app --help`. 

## Test (requires development software)

This application can be tested running
```
./netio-bus-buffered-loopback --bus-dir <bus-dir> <local ip> 12345 0x1000000000008000 53100 0x1000000000000000
./felix-user-app --bus-dir <bus-dir> --ip <local ip> --send-fid 0x1000000000008000 --reply-fid 0x1000000000000000 --message helloworld --iterations 250
```
